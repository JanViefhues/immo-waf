import styles from '../../styles/components/benefits.module.css'
import {useWindowSize} from '../_helper/_windowSize.hook'
import {Device} from '../_helper/_detectDevice.hook'

export default function Benefits() {
    const size = useWindowSize();   
    const width = size.width
    const height = size.height

    let benefit_content = [
        {
            icon: '/icons/fees.png',
            headline: "Vorteil 1",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        },
        {
            icon: '/icons/fees.png',
            headline: "Vorteil 2",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        },
        {
            icon: '/icons/fees.png',
            headline: "Vorteil 3",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        }
    ]
    return (
        <div>
            <div className='container' >
                <div className='row mt-5 mb-5' align='center'>
                    <div className='col-12'>
                        <p className='h2'>Warum unsere Arbeit effektiv ist </p>
                    </div>
                </div>
                <div className='d-flex flex-md-row flex-column justify-content-between mb-5' >
                    {benefit_content.map(ele => {
                        return (
                            <div className='mb-5 mb-md-0' align='center' key={ele.headline}>
                                <div className='d-flex flex-column'>
                                    <img src={ele.icon} className={styles.benefit_icon} />
                                    <span className='mt-4 font-weight-bold'>{ele.headline}</span>
                                    <span style={Device(width) !== 'mobile'? {maxWidth: '250px'} : {maxWidth: ''}}>{ele.text}</span>
                                </div>
                            </div>
                        )
                    })}
                </div>
                <div className='row mt-5 mb-5' align='center'>
                    <div className='col-12'>
                        <p className='h3'>Die Anfragen bearbeiten wir in der Regel innerhalb von 24 Stunden. Der gesamte Verkaufprozess von der Anfrage bis zum Notartermiin ist in nur 7 Tage möglich!</p>
                    </div>
                <button type="button" className="btn btn-primary btn-lg mt-5" style={{marginTop: '5%', margin: '0 auto'}}>Jetzt anfragen</button>
                </div>
            </div>
        </div>
    )
} 