import { useState, useEffect } from 'react'
import {useWindowSize} from '../_helper/_windowSize.hook'
import {Device} from '../_helper/_detectDevice.hook'
import styles from '../../styles/components/mainTeaser.module.css'


export default function MainTeaser(){
    const size = useWindowSize();   
    
    const height = size.height != null ? size.height * 3/4 : 0
    const width = size.width

    return ( 
        <div style={{height: height}} className={styles.mainTeaser}>
               <div className='container' align={Device(width)==="mobile"? "center" : ""}>
               <div style={{paddingTop: '10%'}}>
                   <div className='row'>
                        <div className='col-md-8 col-sm-12' >
                          <p className={styles.mainTeaserText} style={{fontSize: Device(width) === 'mobile'? '2em' : '3em'}}>Wir kaufen Ihre Immobile in Kreis Warendorf</p>
                        </div>
                   </div>
               </div>
               <button type="button" className="btn btn-primary btn-lg" style={{marginTop: '5%'}}>Jetzt anfragen</button>
           </div>
           
        </div>
    )  
}