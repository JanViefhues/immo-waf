import styles from '../../styles/components/benefits.module.css'
import {useWindowSize} from '../_helper/_windowSize.hook'
import {Device} from '../_helper/_detectDevice.hook'

export default function About() {
    const size = useWindowSize();   
    const width = size.width
    const height = size.height

    return (
        <div>
            <div className='container' >
                <div className='row mt-5 mb-5' align='center'>
                    <div className='col-12'>
                        <p className='h2'>Die Spezialisten für den Kreis Warendorf </p>
                    </div>
                </div>
                <div className='d-flex flex-md-row flex-column mb-5' >
                    <div>

                    </div>
                    <div>
                        Wir regional auf den Kreis Warendorf spezialisert. Wir haben Immobilien in 
                        <ul>
                             <li>Ahlen</li>
                            <li>Sendenhorst</li>
                            <li>Drensteinfurt</li>
                            <li>Warendorf</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
} 