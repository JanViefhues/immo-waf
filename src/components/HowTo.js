import styles from '../../styles/components/howto.module.css'
import {Device} from '../_helper/_detectDevice.hook'
import {useWindowSize} from "../_helper/_windowSize.hook"


export default function HowTo(){
    const size = useWindowSize();   
    const width = size.width

    let benefit_content = [
        {
            icon: '/icons/fees.png',
            headline: "Formular ausfüllen und Telefonat vereinbaren",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        },
        {
            icon: '/icons/fees.png',
            headline: "Gespräch vereinbaren",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        },
        {
            icon: '/icons/fees.png',
            headline: "Besichtigungsterim vereinbaren",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        },
        {
            icon: '/icons/fees.png',
            headline: "Angebot erhalten",
            text: 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam'
        }
    ]
    return (
        <div   className={styles.benefit_section_wrapper}>
            <div className='container'>
                <div className='row  mt-5 mb-5 section-padding-top-xl' align='center'>
                    <div className='col-12'>
                        <p className='h2'>Zum Immobilienverkauf in nur vier Schritten</p>
                    </div>
                </div>
                <div className='d-flex flex-md-row flex-column justify-content-between mb-5' >
                    {benefit_content.map(ele => {
                        return (
                            <div className='mb-5 mb-md-0 col-3' align='center' key={ele.headline}>
                                <div className='d-flex flex-column'>
                                    <img src={ele.icon} className={styles.benefit_icon} />
                                    <span className='mt-4 font-weight-bold'>{ele.headline}</span>
                                    <span style={Device(width) !== 'mobile'? {maxWidth: '250px'} : {maxWidth: ''}}>{ele.text}</span>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </div>
            <div className={styles.parallax}></div>
        </div>
    )
}