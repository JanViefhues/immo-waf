import Head from 'next/head'
import styles from '../../styles/Home.module.css'
import Navbar from '../components/Navbar'
import dynamic from 'next/dynamic'

// we need to make this compenten not ssr because we want to work with the resize hook
const NoSSRComponentMainTeaser = dynamic(() => import("../components/MainTeaser"), {
  ssr: false,
});
const NoSSRComponentBenefits = dynamic(() => import("../components/Benefits"), {
  ssr: false,
});
const NoSSRComponentHowTo = dynamic(() => import("../components/HowTo"), {
  ssr: false,
});
const NoSSRComponentAbout = dynamic(() => import("../components/About"), {
  ssr: false,
});

export default function Home() {
  return (
    <>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Navbar />
      <NoSSRComponentMainTeaser />
      <NoSSRComponentBenefits />
      <NoSSRComponentHowTo />
      <NoSSRComponentAbout />

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </>

  )
}
